//
//  main.cpp
//  glManyFigures
//
//  Created by seven on 9/6/13.
//  Copyright (c) 2013 seven. All rights reserved.
//
#include "EventReceiver.h"
#include <irrlicht.h>
#include <assert.h>
#include <iostream>
#include <math.h>
#define MAX_FPS 60
#define IDLE_TIME  (1000 / MAX_FPS)
using namespace irr;
using namespace irr::video;
using namespace irr::core;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;
using namespace std;

IrrlichtDevice *device;
IVideoDriver* driver;
ISceneManager* smgr;
IGUIEnvironment* guienv;
int screenWidth = 800;
int screenHeight = 600;

int main(int argc, const char * argv[])
{
  EventReceiver receiver;
  receiver.init();
  device = createDevice( video::EDT_OPENGL, dimension2d<u32>(screenWidth, screenHeight), 24,
                        false, false, false, &receiver);
  
  if (!device)
    return 1;
  device->setWindowCaption(L"Hello World! - Irrlicht Engine Demo");
  
  driver = device->getVideoDriver();
  smgr = device->getSceneManager();
  guienv = device->getGUIEnvironment();
  f32 fovy = 45.0f;
  f32 fieldOfView = fovy * PI / 180.0;
  matrix4 projection;
  projection.buildProjectionMatrixPerspectiveFovLH(fieldOfView, (f32)driver->getScreenSize().Width/(f32)driver->getScreenSize().Height, 0.0, 500.0);
  matrix4 identity;
  identity.makeIdentity();
  // update the matrices in the driver
  driver->setTransform(ETS_PROJECTION, projection);
  driver->setTransform(ETS_VIEW, identity);
  driver->setTransform(ETS_WORLD, identity);

  scene::ICameraSceneNode *camera = device->getSceneManager()->addCameraSceneNode();
  camera->setPosition(vector3df(0.0,10.0,-50.0));
  camera->setFarValue(1000.0f);
  camera->setNearValue(1.0f);
  
  ISceneNode *cube = smgr->addCubeSceneNode();
  cube->setMaterialFlag(EMF_LIGHTING, true);
  cube->setMaterialFlag(EMF_WIREFRAME, true);
  
  while(device->run())
  {
    u32 time = device->getTimer()->getTime();
    driver->beginScene(true, true, SColor(255,255,255,255));
    smgr->drawAll();
    guienv->drawAll();
    
    //keys
    receiver.startEventProcess();
    if(receiver.keyDown(KEY_KEY_W))
      std::cout<<"hello"<<std::endl;
    if(receiver.keyPressed(KEY_KEY_E))//once
      std::cout<<"hello"<<std::endl;
    receiver.endEventProcess();
    //end keys
    
    camera->setTarget(vector3df(0,0,0));
    camera->setPosition(camera->getPosition() - vector3df(0.2, 0, -0.1));
    driver->endScene();
    device->sleep(IDLE_TIME - (time - device->getTimer()->getTime()));
  }
  device->drop();
  
  return 0;
}

