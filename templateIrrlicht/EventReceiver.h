//
//  EventReceiver.h
//  templateIrrlicht
//
//  Created by seven on 10/15/13.
//  Copyright (c) 2013 none. All rights reserved.
//

#ifndef templateIrrlicht_EventReceiver_h
#define templateIrrlicht_EventReceiver_h
#include "./irrlicht/irrlicht.h"
using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;
/// add  #include "EventReceiver.h"
///Then create an instance: EventReceiver eventReceiver;
/// Then call the initialization fucntion like so: eventReceiver.init();
/// Then inside your Main Game Loop place "eventReceiver.endEventProcess();" in the beginning of your game loop, before anything -
/// that would require input, then put "eventReceiver.startEventProcess();" at the very end of your Main Game Loop.
/// yeah I know it's confusing, but it makes alot more sense in the internals of the class.
class EventReceiver : public IEventReceiver
{
  
protected:
  // Enumeration for UP, DOWN, PRESSED and RELEASED key states. Also used for mouse button states.
  enum keyStatesENUM {UP, DOWN, PRESSED, RELEASED};
  
  // Enumeration for Event Handling State.
  enum processStateENUM {STARTED, ENDED};
  
  // Mouse button states.
  keyStatesENUM mouseButtonState[3]; //Left(0), Middle(1) and Right(2) Buttons.
  
  // Keyboard key states.
  keyStatesENUM keyState[KEY_KEY_CODES_COUNT];
  
  // Mouse X/Y coordinates and Wheel data.
  struct mouseData
  {
    int X;
    int Y;
    float wheel; //wheel is how far the wheel has moved
  };
  struct mouseData mouse;
  
  processStateENUM processState; // STARTED = handling events, ENDED = not handling events
  virtual bool OnEvent(const SEvent& event);
public:
  bool leftMouseReleased();
  bool leftMouseUp();
  bool leftMousePressed();
  bool leftMouseDown();
  bool middleMouseReleased();
  bool middleMouseUp();
  bool middleMousePressed();
  bool middleMouseDown();
  bool rightMouseReleased();
  bool rightMouseUp();
  bool rightMousePressed();
  bool rightMouseDown();
  bool keyPressed(char keycode);
  bool keyDown(char keycode);
  bool keyUp(char keycode);
  bool keyReleased(char keycode);
  bool keyPressed(EKEY_CODE keycode);
  bool keyDown(EKEY_CODE keycode);
  bool keyUp(EKEY_CODE keycode);
  bool keyReleased(EKEY_CODE keycode);
  void endEventProcess();
  void startEventProcess();
  void init();
  float mouseWheel()
  {
    return mouse.wheel;
  }
  
  int mouseX()
  {
    return mouse.X;
  }
  
  int mouseY()
  {
    return mouse.Y;
  }
};

#endif
